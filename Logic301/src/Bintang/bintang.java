package Bintang;

public class bintang {
    public static void main(String args[]) {
        int a,b;
        //no 1
        for (a=1; a<=9; a++) {
            for(b=1; b<=9; b++) {
                if (a==b)
                    System.out.print(a+b-1);
                else
                    System.out.print(" ");
            }System.out.println();
        }
        System.out.println();
        System.out.println();
        //no 2
        for (a=1; a<=9; a++) {
            for(b=1; b<=9; b++) {
                if ((a+b)==(9+1))
                    System.out.print((b-1)*2);
                else
                    System.out.print(" ");
            }System.out.println();
        }
        System.out.println();
        System.out.println();
        //no 3
        for (a=1; a<=9; a++) {
            for(b=1; b<=9; b++) {
                if (a==b)
                    System.out.print(a+b-1);
                else if ((a+b)==(9+1))
                    System.out.print((b-1)*2);
                else
                    System.out.print(" ");
            }System.out.println();
        }
        System.out.println();
        System.out.println();
        //no 4
        for (a=1; a<=9; a++) {
            for(b=1; b<=9; b++) {
                if (a==b)
                    System.out.print(a+b-1);
                else if ((a+b)==(9+1))
                    System.out.print((b-1)*2);
                else if (b==5)
                    System.out.print(a*2-1);
                else
                    System.out.print(" ");
            }System.out.println();
        }
        System.out.println();
        System.out.println();
        //no 5
        for (a=1; a<=9; a++) {
            for(b=1; b<=9; b++) {
                if (a>=b)
                    System.out.print(b*2-1);
                else
                    System.out.print(" ");
            }System.out.println();
        }
        System.out.println();
        System.out.println();
        //no 6
        for (a=9; a>=1; a--) {
            for(b=9; b>=1; b--) {
                if (a+b<=9+1)
                    System.out.print((a-1)*2+" ");
                else
                    System.out.print("  ");
            }System.out.println();
        }
        System.out.println();
        System.out.println();
        //no 7
        for (a=1; a<=9; a++) {
            for(b=1; b<=9; b++) {
                if (a==b)
                    System.out.print(a+b-1);
                else if ((a+b)==(9+1))
                    System.out.print((b-1)*2);
                else if (a<b&&(b+a)<10)
                    System.out.print(" A");
                else if (a>b&&(b+a)>10)
                    System.out.print(" B");
                else
                    System.out.print("  ");
            }System.out.println();
        }
        System.out.println();
        System.out.println();
        //no 8
        for (a=1; a<=9; a++) {
            for(b=1; b<=9; b++) {
                if (a==b)
                    System.out.print(a+b-1);
                else if ((a+b)==(9+1))
                    System.out.print((b-1)*2);
                else if (a>b&&(b+a)<10)
                    System.out.print("A ");
                else if (a<b&&(b+a)>10)
                    System.out.print(" B");
                else
                    System.out.print("  ");
            }System.out.println();
        }
        System.out.println();
        System.out.println();
        //n0 9
        int n = 9;
        int kelipatan = 1;
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(kelipatan + " ");
                if (j < ((n) / 2)) {
                    kelipatan += 2;
                } else {
                    kelipatan -= 2;
                }

            }
            kelipatan = 1;
            System.out.println();
        }
        System.out.println();
        System.out.println();
        //n0 10
        int y = 9;
        int kel = 0;
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < y; j++) {
                System.out.print(kel + " ");
            }
            if (i < ((y) / 2)) {
                kel += 2;
            } else {
                kel -= 2;
            }
            System.out.println();
        }

    }
}
