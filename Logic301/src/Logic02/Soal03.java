package Logic02;
import java.util.Scanner;
public class Soal03 {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.print("Please enter the value of n: ");
            int n = input.nextInt();
            System.out.print("Please enter the value of n2: ");
            int n2 = input.nextInt();
            int helper = n2;

            // Prerequisite
            int[][] array2dInteger = new int[2][n];

            for (int i = 0; i < array2dInteger.length; i++) {
                for (int j = 0; j < array2dInteger[0].length; j++) {
                    if (i == 0) {
                        array2dInteger[i][j] = j;
                    } else {
                        if (j < 4) {
                            array2dInteger[i][j] = helper;//menghitung sampai 24
                            helper = helper * 2;
                        } else {
                            array2dInteger[i][j] = helper / (n2 + 1); //menghitung 48 dibagi 4 menjadi 12
                            helper = helper / (n2 - 1); //12 dibagi 2 terus meneurs
                        }
                    }
                }
            }
            // Print Array
            for (int i = 0; i < array2dInteger.length; i++) {
                for (int j = 0; j < array2dInteger[0].length; j++) {
                    System.out.print(array2dInteger[i][j] + " ");
                }
                System.out.println();
            }
        }
    }
}
