package Logic02;
import java.util.Scanner;
public class Soal10 {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.println("Please enter the value of n: ");
            int n = input.nextInt();
            System.out.println("Please enter the value of n2: ");
            int n2 = input.nextInt();
            int helper=1,helper1=1;
            // Prerequisite
            int[][] array2dInteger = new int[3][n];
            for (int i = 0; i < array2dInteger.length; i++) {
                for (int j = 0; j < array2dInteger[0].length; j++) {
                    if ( i == 0 )
                    {
                        array2dInteger[i][j] = j;
                    }
                    if ( i == 1 )
                    {
                        array2dInteger[i][j] = j*3;
                    }
                    if ( i == 2 )
                    {
                        array2dInteger[i][j] = j*4;
                    }
                }
            }

            // Cetak Array

            for (int i = 0; i < array2dInteger.length; i++) {
                for (int j = 0; j < array2dInteger[0].length; j++) {
                    System.out.print(array2dInteger[i][j] + " ");
                }
                System.out.println();
            }
        }
    }
}

