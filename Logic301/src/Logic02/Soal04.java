package Logic02;
import java.util.Scanner;
public class Soal04 {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.print("Please enter the value of n: ");
            int n = input.nextInt();
            System.out.print("Please enter the value of n2: ");
            int n2 = input.nextInt();
            int helper=1,helper1=5;

            // Prerequisite
            int[][] array2dInteger = new int[2][n];

            for (int i = 0; i < array2dInteger.length; i++) {
                for (int j = 0; j < array2dInteger[0].length; j++) {
                    if (i == 0){
                        array2dInteger[i][j] = j;
                    }
                    else if ( j%2 == 0)//mengisi index kelipatan 2 dari no 1 dengan angka normal
                    {
                        array2dInteger[i][j] = helper;
                        helper=helper+1;
                    }
                    else {//mengisi index kelipatan 2 dari no 2 dengan penjumlahan 5
                        array2dInteger[i][j] = helper1;
                        helper1=helper1+5;
                    }
                }
            }
            // Print Array
            for (int i = 0; i < array2dInteger.length; i++) {
                for (int j = 0; j < array2dInteger[0].length; j++) {
                    System.out.print(array2dInteger[i][j] + " ");
                }
                System.out.println();
            }
        }
    }
}
