package Logic02;
import java.util.Scanner;
public class Soal07 {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            System.out.println("Please enter the value of n: ");
            int n = input.nextInt();
            System.out.println("Please enter the value of n2: ");
            int n2 = input.nextInt();

            // Prerequisite
            int[][] array2dInteger = new int[3][n];
            for (int i = 0; i < array2dInteger.length; i++) {
                for (int j = 0; j < array2dInteger[0].length; j++) {
                    if ( i == 0 )
                    {
                        array2dInteger[i][j] = j;//menghitung normal baris 1 kekanan
                    }
                    if ( i == 1 )
                    {
                        array2dInteger[i][j] = j+n;//menghitung normal baris 2 ditambah n
                    }
                    if ( i == 2 )
                    {
                        array2dInteger[i][j] = j+n+n;//menghitung normal baris 3 doble n
                    }

                }
            }

            // Cetak Array

            for (int i = 0; i < array2dInteger.length; i++) {
                for (int j = 0; j < array2dInteger[0].length; j++) {
                    System.out.print(array2dInteger[i][j] + " ");
                }
                System.out.println();
            }
        }
    }
}
