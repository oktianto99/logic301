package Logic03;
import java.util.Scanner;
public class Soal10 {
    public static void Resolve() {
        Scanner in = new Scanner(System.in);
        //Input score Alice n Bob ke dalam array
        //a milik Alice n b milik Bob
        int alice0 = in.nextInt();
        int alice1 = in.nextInt();
        int alice2 = in.nextInt();
        int bob0 = in.nextInt();
        int bob1 = in.nextInt();
        int bob2 = in.nextInt();

        //Inisialisasi
        int aliceCount = 0;
        int bobCount = 0;

        //Perhitungan score
        //0
        if (alice0 > bob0) {
            aliceCount++;
        } else if (alice0 < bob0) {
            bobCount++;
        }
        //1
        if (alice1 > bob1) {
            aliceCount++;
        } else if (alice1 < bob1) {
            bobCount++;
        }
        //2
        if (alice2 > bob2) {
            aliceCount++;
        } else if (alice2 < bob2) {
            bobCount++;
        }

        System.out.println(aliceCount + " " + bobCount);
    }
}
