package Logic03;

import java.util.Scanner;
public class Soal01 {
    public static void Resolve()
    {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the number sequence: ");
        System.out.print("a: ");
        String numberString = input.nextLine();
        System.out.print("b: ");
        String number1String = input.nextLine();

        String[] numberStringArray = numberString.split(" ");
        int[] numberInt = new int[numberStringArray.length];
        String[] number1StringArray = number1String.split(" ");
        int[] number1Int = new int[number1StringArray.length];
        int answer = 0;

        for (int i = 0; i < numberInt.length; i++) {
            numberInt[i] = Integer.parseInt(numberStringArray[i]);
            number1Int[i] = Integer.parseInt(number1StringArray[i]);
        }

        for (int i = 0; i < numberInt.length; i++) {
            answer = numberInt[i] + number1Int[i];
        }

        System.out.println("The answer is: " + answer);
    }
}
