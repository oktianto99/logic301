package Logic03;
import java.util.Scanner;
public class Soal05 {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        // Input the value
        System.out.println("Enter the number sequence: ");
        String[] numberString = input.nextLine().split(" ");
        int[] numberInt = new int[numberString.length];

        for (int i = 0; i < numberInt.length; i++) {
            numberInt[i] = Integer.parseInt(numberString[i]);
        }
        int positive = 0;
        int negative = 0;
        int neutral= 0;
        for (int i = 0; i < numberInt.length; i++) {
            if(numberInt[i]>0){
                positive+=1;
            }else if(numberInt[i]<0){
                negative+=1;
            }else{
                neutral+=1;
            }
        }
        float a = (float) positive/6;
        float b = (float) negative/6;
        float c = (float) neutral/6;
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
}
