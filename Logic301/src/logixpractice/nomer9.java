package logixpractice;
import java.util.Scanner;
public class nomer9 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        //1. Delapan jam pertama: 1.000/jam
        //2. Lebih dari 8 jam s.d. 24 jam: 8.000 flat
        //3. Lebih dari 24 jam: 15.000/24 jam dan selebihnya mengikuti ketentuan pertama dan kedua
        //deklarasi
        double[] jamMasuk = new double[1];
        double[] menitMasuk = new double[1];

        double[] jamKeluar = new double[1];
        double[] menitKeluar = new double[1];

        String[] bulanTahun = new String[1];
        double[] tanggalMasuk = new double[1];
        double[] tanggalKeluar = new double[1];

        //input data
        for (int i=0; i<1; i++) {
            System.out.print("Input bulan dan tahun masuk parkir (ex. November 2022): ");
            bulanTahun[i] = input.nextLine();
            System.out.print("Tanggal masuk: ");
            tanggalMasuk[i] = input.nextDouble();
            System.out.print("Jam Masuk: ");
            jamMasuk[i] = input.nextDouble();
            System.out.print("Menit Masuk: ");
            menitMasuk[i] = input.nextDouble();
            System.out.print("Jam Keluar: ");
            jamKeluar[i] = input.nextDouble();
            System.out.print("Menit Keluar: ");
            menitKeluar[i] = input.nextDouble();
        }
        //hitung lamanya parkir
        int i=0;
        Double jamParkir = (jamKeluar[i]-jamMasuk[i]);
        Double menitParkir = (menitKeluar[i]-menitMasuk[i]);
        if(menitParkir<0){
            jamParkir = jamParkir-1;
            menitParkir = menitParkir+60;
        }
        System.out.println("Lamanya parkir: " +jamParkir+ " jam " +menitParkir+ " menit");

        //bulatkan lamanya parkir
        double waktuParkir = Math.ceil((((jamKeluar[i]*60)+menitKeluar[i])-((jamMasuk[i]*60)+menitMasuk[i]))/60);
        System.out.println("Dibulatkan menjadi " +waktuParkir+ " jam");

        double tarif;
        //hitung tarif
        if (waktuParkir <= 8){
            tarif=waktuParkir*1000;
            System.out.print("Total tarif parkir Rp. ");
            System.out.println(tarif);
        } else if (waktuParkir > 8 && waktuParkir <= 24) {
            tarif=8000;
            System.out.print("Total tarif parkir Rp. ");
            System.out.println(tarif);
        } else if (waktuParkir > 24) {
            double waktuParkir24=waktuParkir-24;
            if(waktuParkir24<=8){
                tarif=(waktuParkir24*1000)+15000;
                System.out.print("Total tarif parkir Rp. ");
                System.out.println(tarif);
            } else if (waktuParkir24 > 8 && waktuParkir24 < 24){
                tarif = 15000 + 8000;
                System.out.print("Total tarif parkir Rp. ");
                System.out.println(tarif);
            }
        }
    }
}
