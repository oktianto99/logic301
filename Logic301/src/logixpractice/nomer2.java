package logixpractice;
import java.util.Scanner;
public class nomer2 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.println("Fuel Usage");
        System.out.println("1. Grosir X");
        System.out.println("2. Toko 1");
        System.out.println("3. Toko 2");
        System.out.println("4. Toko 3");
        System.out.println("5. Toko 4");
        System.out.println("Masukan Rute: ");
        String urutan = input.nextLine();
        String [] rute = urutan.split("-");//dari suati kalimat mengabaikan (-)
        int [] array = new int[rute.length];
        int distance = 0;
        int waktutoko=0;

        int i;
        for(i=0; i<rute.length; i++){
            array[i] =Integer.parseInt(rute[i]);//parseInt = merubah dtaa string menjadi integer agar dapat membaca for bawah
        }
        for(i = 0; i < array.length - 1; ++i) {
            if (array[i] == 1 && array[i + 1] == 2) {
                distance += 500;
                waktutoko += 10;
            } else if (array[i] == 1 && array[i + 1] == 3) {
                distance += 2000;
                waktutoko += 10;
            } else if (array[i] == 1 && array[i + 1] == 4) {
                distance += 3500;
                waktutoko += 10;
            } else if (array[i] == 1 && array[i + 1] == 5) {
                distance += 5000;
                waktutoko += 10;
            } else if (array[i] == 2 && array[i + 1] == 1) {
                distance += 500;
                waktutoko += 10;
            } else if (array[i] == 2 && array[i + 1] == 3) {
                distance += 1500;
                waktutoko += 10;
            } else if (array[i] == 2 && array[i + 1] == 4) {
                distance += 3000;
                waktutoko += 10;
            } else if (array[i] == 2 && array[i + 1] == 5) {
                distance += 3500;
                waktutoko += 10;
            } else if (array[i] == 3 && array[i + 1] == 1) {
                distance += 2000;
                waktutoko += 10;
            } else if (array[i] == 3 && array[i + 1] == 2) {
                distance += 1500;
                waktutoko += 10;
            } else if (array[i] == 3 && array[i + 1] == 4) {
                distance += 1500;
                waktutoko += 10;
            } else if (array[i] == 3 && array[i + 1] == 5) {
                distance += 3000;
                waktutoko += 10;
            } else if (array[i] == 4 && array[i + 1] == 1) {
                distance += 3500;
                waktutoko += 10;
            } else if (array[i] == 4 && array[i + 1] == 2) {
                distance += 3000;
                waktutoko += 10;
            } else if (array[i] == 4 && array[i + 1] == 3) {
                distance += 1500;
                waktutoko += 10;
            } else if (array[i] == 4 && array[i + 1] == 5) {
                distance += 1500;
                waktutoko += 10;
            } else if (array[i] == 5 && array[i + 1] == 1) {
                distance += 5000;
                waktutoko += 10;
            } else if (array[i] == 5 && array[i + 1] == 2) {
                distance += 4500;
                waktutoko += 10;
            } else if (array[i] == 5 && array[i + 1] == 3) {
                distance += 3000;
                waktutoko += 10;
            } else if (array[i] == 5 && array[i + 1] == 4) {
                distance += 1500;
                waktutoko += 10;
            }
        }
        int waktuTempuh = distance/500;//500 adalah hasil dari 30km/jam menjadi m/m
        //t=jarak/kecepatan 30x1000/60=500
        int waktuDrop = waktutoko-10;

        System.out.println("Waktu Tempuh = " + waktuTempuh + " minutes");
        System.out.println("Waktu Drop Barang = " + waktuDrop + " minutes");
        System.out.println("Total Waktu = " + (waktuTempuh+waktuDrop) + " minutes");
    }
}
