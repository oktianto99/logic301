package logixpractice;
import java.util.Scanner;
public class nomer5 {
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        System.out.print("Konversi Waktu");
        System.out.print("Masukan Jam (Ex: 13:30 ) : ");
        String StringJam = input.nextLine(); //Input waktu
        String[] ArrayStringJam = StringJam.split(" "); //Split stringjam antara AM/PM dan Jam

        if(ArrayStringJam.length==1){ //Kondisi ketika inputan panjangnya 1 maka untuk 12H ke 24H
            String StringJam24 = ArrayStringJam[0];
            String[] ArrayStringJam24 = StringJam24.split(":"); //Split Waktu berdasarkan ":"

            int jam24H = Integer.parseInt(ArrayStringJam24[0]); // Mengembalikan nilai jam string ke int
            int menit24H = Integer.parseInt(ArrayStringJam24[1]); // Mengembalikan nilai menit string ke int

            if (jam24H < 12 && menit24H <= 59) {
                System.out.print("Output : " + jam24H + ":" + menit24H + " AM");
            } else if (jam24H > 12 && jam24H <= 23 && menit24H <= 59) {
                System.out.print("Output : " + (jam24H - 12) + ":" + menit24H + " PM");
            } else if ((jam24H > 24) || (menit24H > 59)) {
                System.out.print("Format tidak valid");
            }
        } else{ //Kondisi ketika inputan panjangnya 1 maka untuk 24H ke 12H
            String StringJam12 = ArrayStringJam[0];
            String[] ArrayStringJam12 = StringJam12.split(":"); //Split Waktu berdasarkan ":"

            int jam12H = Integer.parseInt(ArrayStringJam12[0]); // Mengembalikan nilai jam string ke int
            int menit12H = Integer.parseInt(ArrayStringJam12[1]); // Mengembalikan nilai menit string ke int

            String StringAM = "AM";
            String StringPM = "PM";

            String AMPM = ArrayStringJam[1]; // Menyimpan string AM/PM

            if (jam12H < 12 && menit12H <= 59 && AMPM.toLowerCase().equals(StringAM.toLowerCase())) {
                System.out.print("Output : " + jam12H + ":" + menit12H);
            } else if (jam12H == 12 && menit12H <= 59 && AMPM.toLowerCase().equals(StringAM.toLowerCase())) {
                System.out.print("Output : " + "00" + ":" + menit12H);
            } else if (jam12H < 12 && menit12H <= 59 && AMPM.toLowerCase().equals(StringPM.toLowerCase())) {
                System.out.print("Output : " + (jam12H + 12) + ":" + menit12H);
            } else if ((jam12H > 12) || (menit12H > 59)) {
                System.out.print("Format tidak valid");
            }
        }
        /*
        System.out.print("KONVERSI WAKTU");
        System.out.print("\nPiliham Konversi : \n");
        System.out.print("1. 24H ke 12H \n");//mencapai 12 perlu dikurangi 12
        System.out.print("2. 12H ke 24H \n");//mencapai 12 perlu ditambah 12
        char ulangMenu;
        do {
            Scanner input = new Scanner(System.in);
            System.out.print("Pilih konversi waktu : ");
            int inputPilih = input.nextInt();
            switch(inputPilih) {
                case 1://pada 24 to 12 hanya membutuhkan intputan 00-23 akan menjadi waktu 12jam dalam AM/PM
                    System.out.print("Masukan Jam : ");
                    int jam24 = input.nextInt();
                    System.out.print("Masukan menit : ");
                    int menit24 = input.nextInt();
                    if(menit24>=60){//jika menit melebihi 60 maka perlu adegan
                        jam24=jam24+(menit24/60);//menambhak
                        menit24=menit24%60;
                    }
                    System.out.print(("Pukul ") + jam24 + (":")+ menit24);
                    if(jam24 < 12  ) {//dari 24 jam ke 12 jam
                        System.out.print("\nKonversi waktu " + jam24 +":" + menit24 + " am");
                    }
                    else if(jam24 >= 12&&jam24<=23) {//dari 24 jam ke 12 jam bila lebih dari 12 jam perlunya mengurangi 12 jam karene di konversi ke 12 jam
                        System.out.print("\nKonversi waktu " + (jam24 - 12) +":" + menit24 + " pm");
                    }
                    else if(jam24 >= 24) {//dari 24 jam ke 12 jam bila lebih dari 12 jam perlunya mengurangi 12 jam karene di konversi ke 12 jam
                        System.out.print("\nKonversi waktu " + (jam24-jam24) +":" + menit24 + " pm");
                    }
                    break;
                case 2://pada 12 to 24 hanya membutuhkan intputan 00-11 akan menjadi waktu 24jam
                    String StringAM = "AM";
                    String StringPM = "PM";
                    System.out.print("Masukan Jam : ");
                    int jam12 = input.nextInt();
                    System.out.print("Masukan menit : ");
                    int menit12 = input.nextInt();
                    System.out.print("AM/PM : ");
                    String AMPM = input.next();
                    if(menit12>=60){//jika menit melebihi 60 maka perlu adegan
                        jam12=jam12+(menit12/60);//menambhak
                        menit12=menit12%60;
                    }
                    System.out.print(("Pukul ") +jam12 + (":")+ menit12 +(" ")+AMPM);//dalam 24 to 12 kondisi AM/PM akan dihilangkan
                    //kondisi am
                    if(AMPM.toLowerCase().equals(StringAM.toLowerCase())&&jam12<=23) {//merubah string AM dalam ukuran kecil
                        System.out.print("\nKonversi waktu " + jam12 +":" + menit12);//AM/PM dihilangkan
                    }
                    else if(AMPM.toLowerCase().equals(StringAM.toLowerCase())&&jam12>=24) {//merubah string AM dalam ukuran kecil
                        System.out.print("\nKonversi waktu " + (jam12-jam12)+":" + menit12);//AM/PM dihilangkan
                    }
                    //kondisi pm
                    else if(AMPM.toLowerCase().equals(StringPM.toLowerCase())&&jam12<12) {//merubah string PM dalam ukuran kecil
                        System.out.print("\nKonversi waktu " + (jam12 + 12) +":" + menit12);
                    }else if(AMPM.toLowerCase().equals(StringPM.toLowerCase())&&jam12==12) {//merubah string PM dalam ukuran kecil
                        System.out.print("\nKonversi waktu " + (jam12 - jam12) +":" + menit12);
                    }else if(AMPM.toLowerCase().equals(StringPM.toLowerCase())&&jam12>=13&&jam12<=24) {//merubah string PM dalam ukuran kecil
                        System.out.print("\nKonversi waktu " + (jam12 - 12) +":" + menit12);
                    }else if(AMPM.toLowerCase().equals(StringPM.toLowerCase())&&jam12>=25) {//merubah string PM dalam ukuran kecil
                        System.out.print("\nKonversi waktu " + (jam12 - jam12) +":" + menit12);
                    }
                    break;
                default:
                    System.out.print("Inputan salah. Pilih angka 1 atau 2  !!!!");
            }
            do {
                System.out.print("\nMasih ingin input (y/n)? ");
                ulangMenu = input.next().charAt(0);
                if (ulangMenu != 'n' && ulangMenu != 'y') {
                    System.out.println("Maaf inputan tidak sesuai");
                }
            } while(ulangMenu != 'n' && ulangMenu != 'y');//masuk ke perulangan selain n dan y
        } while(ulangMenu != 'n' && ulangMenu == 'y');//jika y ngulangi, jika n maka udahan
        */
    }
}

