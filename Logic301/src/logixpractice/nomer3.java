package logixpractice;
import java.util.Scanner;
import java.util.Arrays;
public class nomer3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input banyak record penjualan : ");
        int banyakInput = input.nextInt();
        input.nextLine();

        String[] recordPenjualan = new String[banyakInput];
        System.out.println();

        //input record penjualan
        System.out.println("Penulisan record (ex. apel:3)");
        for (int i = 0; i < banyakInput; i++) {
            System.out.printf("Input record ke-" +i+ "= ");
            String inputPenjualanBuah = input.nextLine();
            if (inputPenjualanBuah.contains(":")) {
                recordPenjualan[i] = inputPenjualanBuah;
            } else {
                System.out.println("Salah format! Ulangi input dari awal.");
            }
        }
        System.out.println();

        //split ":" dalam inputan
        String[] buahList;
        String[][] recordPenjualanSplit = new String[banyakInput][2];
        for (int i = 0; i < banyakInput; i++) {
            buahList = recordPenjualan[i].split(":");
            for (int j = 0; j < buahList.length; j++) {
                recordPenjualanSplit[i][j] = buahList[j];
            }
        }

        String[] buah = new String[banyakInput];

        //to lower case
        for (int i = 0; i < banyakInput; i++) {
            buah[i] = recordPenjualanSplit[i][0].toLowerCase();
        }

        //data yang sama dijadikan 1 atau remove duplicate
        buahList = (String[]) Arrays.stream(buah).distinct().toArray((x$0) -> {
            return new String[x$0];
        });

        String[][] summaryRecord = new String[buahList.length][2];

        Arrays.sort(buahList);
        for (int i = 0; i < buahList.length; i++) {
            int sumPenjualan = 0;
            for (int j = 0; j < recordPenjualanSplit.length; j++) {
                if (recordPenjualanSplit[j][0].toLowerCase().equals(buahList[i])) {
                    sumPenjualan += Integer.parseInt(recordPenjualanSplit[j][1]);
                    summaryRecord[i][0] = buahList[i];
                    summaryRecord[i][1] = Integer.toString(sumPenjualan);
                }
            }
        }

        System.out.println("==Summary Record==");
        for (int i = 0; i < summaryRecord.length; i++) {
            for (int j = 0; j < summaryRecord[0].length; j++) {
                System.out.print(summaryRecord[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println();
        /*Scanner input = new Scanner(System.in);
        int jumlahapel=0, jumlahjeruk=0,jumlahmangga=0,jumlahpisang=0,jumlah;
        char [] buah= new char[0];
        String buah1= "apel",buah2= "jeruk",buah3= "mangga",buah4= "pisang",buahbuah;
        String answer = "Y";
        while (answer.toUpperCase().equals("Y")){
            System.out.print("Nama buah = ");
            buahbuah = input.nextLine();
            String stringConvert = buahbuah.toLowerCase();//toLowerCase = merubah huruf menjadi kecil
            char[] charArray = stringConvert.toCharArray();//toCharArray= merubah kalimat menjadi perkata
            Arrays.sort(charArray);//mengurutkan abjad yang acak menjadi rapi
            switch (buahbuah){
                case "apel":
                    System.out.print("Jumlah buah = ");
                    jumlah = input.nextInt();
                    jumlahapel+=jumlah;
                    break;
                case "jeruk":
                    System.out.print("Jumlah buah = ");
                    jumlah = input.nextInt();
                    jumlahjeruk+=jumlah;
                    break;
                case "mangga":
                    System.out.print("Jumlah buah = ");
                    jumlah = input.nextInt();
                    jumlahmangga+=jumlah;
                    break;
                case "pisang":
                    System.out.print("Jumlah buah = ");
                    jumlah = input.nextInt();
                    jumlahpisang+=jumlah;
                    break;
                default:
                    System.out.println("Bukan");
                    break;
            }
            System.out.println("Lagi? (Y/N)");
            input.nextLine();
            answer = input.nextLine();
        }
        System.out.println("Nama Buah ("+ buah1 + ") Jumlah=" +jumlahapel);
        System.out.println("Nama Buah ("+ buah2 + ") Jumlah=" +jumlahjeruk);
        System.out.println("Nama Buah ("+ buah3 + ") Jumlah=" +jumlahmangga);
        System.out.println("Nama Buah ("+ buah4 + ") Jumlah=" +jumlahpisang);
        /*int genap = 0, ganjil = 1;
        int[] array = new int[batasan];//menampung jumlah baris
        int[] arraygenap = new int[batasan];
        int[] arrayganjil = new int[batasan];
        int[] arraySum = new int[batasan];
        int i;
        for(i = 0; i < array.length; ++i) {//masuk perulangan pada jumlah n yang ditampung pada array
            System.out.print(genap + " ");
            arraygenap[i] = genap;//nilai genap ditampung
            genap = genap + 2;
        }
        System.out.println();
        for(i = 0; i < array.length; ++i) {//masuk perulangan pada jumlah n yang ditampung pada array
            System.out.print(ganjil + " ");
            arrayganjil[i] = ganjil;//nilai ganjil ditampung
            ganjil = (ganjil+2);
        }
        System.out.println();
        System.out.println();
        for(i = 0; i < array.length; ++i) {
            arraySum[i] = arraygenap[i] + arrayganjil[i];
            System.out.print(arraySum[i] + " ");
        }*/
    }
}
