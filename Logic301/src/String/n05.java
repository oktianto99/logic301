package String;
import java.util.*;
public class n05 {//menentukan nama string kalimat acak namun harus ada kalimat terpilih (hackerrank)
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int q = in.nextInt();
        for (int a = 0; a < q; a++) {
            String s = in.next();
            int j = 0;    //keeps track of the letter index of the word "hackerrank"
            String hackerRank = "hackerrank";
            for (int i = 0; i < s.length(); i++) {
                if (j < hackerRank.length() && s.charAt(i) == hackerRank.charAt(j)) {
                    j++;
                }
            }
            //checks if all the letters of the word "hackerrank" have occured in the word given
            if (j == hackerRank.length())
                System.out.println("YES");
            else
                System.out.println("NO");
        }
    }
}
