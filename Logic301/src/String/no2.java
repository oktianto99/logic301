package String;
import java.io.*;
import java.util.*;
import java.util.Scanner;
public class no2 {
    public static void printStrongNess(String input)
    {
        // Checking lower alphabet in string
        int n = input.length();
        boolean hasLower = false, hasUpper = false,
                hasDigit = false, specialChar = false;
        Set<Character> set = new HashSet<Character>(
                Arrays.asList('!', '@', '#', '$', '%', '^', '&',
                        '*', '(', ')', '-', '+'));
        for (char i : input.toCharArray())
        {
            if (Character.isLowerCase(i))
                hasLower = true;
            if (Character.isUpperCase(i))
                hasUpper = true;
            if (Character.isDigit(i))
                hasDigit = true;
            if (set.contains(i))
                specialChar = true;
        }
        // Strength of password
        System.out.print("Strength of password: ");
        if (hasDigit && hasLower && hasUpper && specialChar
                && (n >= 6))
            System.out.print(" Strong");
        else if ((hasDigit || hasLower || hasUpper)
                && (n < 6))
            System.out.print(" 3");
        else if ((hasLower || hasUpper || specialChar)
                && (n < 6))
            System.out.print(" 3");
        else
            System.out.print(" 1");
    }
    /*static void help()
    {
        // ini variabel lokal
    String nama = "Petani Kode";
    //String numberString = input.nextLine();
    // mengakses variabel global di dalam fungso help()
        System.out.println("Nama: " + nama);
        System.out.println("Versi: " + nama);
    }*/
    // Driver Code
    public static void main(String[] args)
    {
        //Scanner input;
        //help();

        String input = "1HackerRank";
        printStrongNess(input);
        //System.out.println("The answer is: ");
    }
}
