package String;
import java.util.*;
public class no2dinamis {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Input password: ");
        String password = input.next();
        //String passwordStr = "";

        int passwordLength = password.length();

        boolean hasLower = false, hasUpper = false,
                hasDigit = false, specialChar = false;
        Set<Character> set = new HashSet<Character>(
                Arrays.asList('!', '@', '#', '$', '%', '^', '&',
                        '*', '(', ')', '-', '+'));
        for (char i : password.toCharArray())//membaca perkarakter dari variable pass ke i sebanyak jumlah inputan karakter
        {
            if (Character.isLowerCase(i))//(isLowerCase) hufur kecil
                hasLower = true;
            if (Character.isUpperCase(i))//(isUpperCase) hufur besar
                hasUpper = true;
            if (Character.isDigit(i))//(isDigit) hufur angka
                hasDigit = true;
            if (set.contains(i))//(specialChar) hufur karakter
                specialChar = true;
        }
        // Strength of password
        System.out.print("Strength of password:- ");
        if (hasDigit && hasLower && hasUpper && specialChar
                && (passwordLength >= 6))
            System.out.print(" Strong");
        else if ((hasDigit || hasLower || hasUpper)
                && (passwordLength < 6))
            System.out.print(" 3");
        else if ((hasLower || hasUpper || specialChar)
                && (passwordLength < 6))
            System.out.print(" 3");
        else
            System.out.print(" 1");
    }
}
