package Soalbaru;
import java.util.Scanner;
public class no6 {
    public static void main(String[] args) {
        String pin = "tianlah";
        long saldo = 0L;
        char ulangTransaksi;
        do {
            Scanner input = new Scanner(System.in);
            System.out.print("Masukan PIN = ");
            String inputPin = input.nextLine();
            if (pin.equals(inputPin)) {//equals membandingkan inputPin dan pin
                System.out.println("Masukan Nominal Setor tunai");
                long inputSetor = (long)input.nextInt();//panjang nominal
                saldo += inputSetor;//tiap inputan setoran disimpan di saldo
                System.out.println("Setor Tunai Berhasil");
                System.out.println("Saldo anda saat ini : " + saldo);
                System.out.println();
                System.out.println("1.Antar Rekening \n2.Antar Bank");
                System.out.println("Pilih Jenis Transfer : ");
                int menuTransfer = input.nextInt();//input pilihan
                if (menuTransfer == 1) {
                    System.out.println("Transfer Sesama Bank");
                    System.out.println("Masukan Nomor Rekening Tujuan");
                    long noRek1 = input.nextLong();
                    System.out.println("Masukan Nominal Transfer");
                    long nominalTransfer = input.nextLong();
                    if (nominalTransfer >= saldo) {
                        System.out.println("Saldo Tidak Cukup");
                    } else if (nominalTransfer <= saldo) {
                        System.out.println("Tranfer ke Nomor Rekening : " + noRek1);
                        System.out.println("Jumlah Nominal transfer : " + nominalTransfer);
                        System.out.println("Transfer berhasil");
                        saldo -= nominalTransfer;//tiap nominal tf disimpan di saldo
                        System.out.println("Saldo Anda saat ini : " + saldo);
                    }
                } else if (menuTransfer == 2) {
                    System.out.println("Masukan kode BAnk");
                    int kodeBank = input.nextInt();
                    System.out.println("Masukan Nomor Rekening Tujuan");
                    long noRek1 = input.nextLong();
                    System.out.println("Masukan Nominal Transfer");
                    long nominalTransfer = input.nextLong();
                    if (nominalTransfer + 7500L >= saldo) {
                        System.out.println("Saldo Tidak Cukup");
                    } else if (nominalTransfer <= saldo + 7500L) {
                        System.out.println("Tranfer ke Nomor Rekening : " + noRek1);
                        System.out.println("Jumlah Nominal transfer : " + nominalTransfer);
                        System.out.println("Transfer berhasil");
                        saldo -= nominalTransfer + 7500L;//tiap nominal tf disimpan di saldo
                        System.out.println("Saldo Anda saat ini : " + saldo);
                    }
                }
            } else {
                System.out.println("PIN yang Anda masukan salah");
            }
            System.out.println("\nIngin transaksi lagi! (y/n)");
            ulangTransaksi = input.next().charAt(0);
        } while(ulangTransaksi == 'y' && ulangTransaksi != 'n');
    }
}
