package Soalbaru;
import java.util.Scanner;
public class no3 {
    static int hitungSumKuadrat(int getTampungangka) { //fungsi menghitung hasil kuadrat input
        int sumKuadrat = 0;
        String numberString = Integer.toString(getTampungangka); //=deret -> string jadi '100'
        char[] charArray = numberString.toCharArray(); // setelah diconvert ke char jadi ['1','0','0'] (ex.100)
        int[] digitArray = new int[charArray.length];

        int i;
        for (i = 0; i < charArray.length; ++i) {                        // int -> [1,0,0]
            digitArray[i] = Character.getNumericValue(charArray[i]); //mengembalikan nilai awal -dari string 1 -> int 1
        }

        for (i = 0; i < digitArray.length; ++i) {
            sumKuadrat = (int) (sumKuadrat + Math.pow((double) digitArray[i], 2)); //jumlah hasil kuadrat
        }                          //Math.pow hanya menerima (double)              // ex.100 -> [o]1^2 + [1]0^2 + [2]0^2 = 1

        return sumKuadrat;
    }

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Cetak berapa si Angka 1? : "); //input sebanyak n
        int inputNumber = input.nextInt();
        int deret = 100; //awal
        int helper = 0; //bantu menyimpan si Angka 1 sebanyak n

        while (true) { //eksekusi terus karena diberi kondisi true
            int tampungAngka = deret;

            do {
                tampungAngka = hitungSumKuadrat(tampungAngka);
            } while (tampungAngka >= 10); //min. hasil perhitungan dari 103  n 1^2+0^2=1

            if (tampungAngka == 1) {
                ++helper;
                System.out.println(+deret + " adalah Si Angka 1");
            }

            if (helper == inputNumber) {
                break; //stop looping
                //return;
            }
            ++deret;
        }
    }
}
