package Soalbaru;

import java.util.Scanner;

public class no5 {
    public static void main(String[] args) {
            float totalLaki = 0.0F;
            float totalPerempuan = 0.0F;
            float totalRemaja = 0.0F;
            float totalAnak = 0.0F;
            float totalBalita = 0.0F;
            System.out.print("1. Laki-laki dewasa");//1 orang 2 porsi
            System.out.print("\n2. Perempuan dewasa");//1 orang 1 porsi
            System.out.print("\n3. Remaja");//2 orang 2 porsi
            System.out.print("\n4. Anak-anak");//1 orang 1/2 porsi
            System.out.print("\n5. Balita");//1 orang 1 porsi
            char ulangMenu;
            do {
                System.out.print("\nPilih rentang usia : ");
                Scanner input = new Scanner(System.in);
                int inputCase = input.nextInt();
                switch (inputCase) {
                    case 1:
                        System.out.print("Masukan jumlah orang laki-laki dewasa : ");
                        int lakiDewasa = input.nextInt();
                        totalLaki += (float)lakiDewasa;//dirubah data flot agar bisa menghasilkan koma
                        break;
                    case 2:
                        System.out.print("Masukan jumlah orang perempuan dewasa : ");
                        int perempuanDewasa = input.nextInt();
                        totalPerempuan += (float)perempuanDewasa;
                        break;
                    case 3:
                        System.out.print("Masukan jumlah orang remaja : ");
                        int remaja = input.nextInt();
                        totalRemaja += (float)remaja;
                        break;
                    case 4:
                        System.out.print("Masukan jumlah orang anak-anak : ");
                        int anak = input.nextInt();
                        totalAnak += (float)anak;
                        break;
                    case 5:
                        System.out.print("Masukan jumlah orang balita : ");
                        int balita = input.nextInt();
                        totalBalita += (float)balita;
                        break;
                    default:
                        System.out.print("Inputan salah. Pilih angka 1 sampai 5!");
                }
                do {
                    System.out.print("\nMasih ingin input (y/n)? ");
                    ulangMenu = input.next().charAt(0);//charAt(0) untuk mendapatkam karakter dengan (0) adalah index 0
                    //njot di simpan nng ulangMenu
                    if (ulangMenu != 'n' && ulangMenu != 'y') { //selain huruf n atau y
                        System.out.println("Maaf inputan tidak sesuai");
                    }
                } while(ulangMenu != 'n' && ulangMenu != 'y');//berulang selama huruf selain n atau y
                //jika y akan false dan eksekusi bawahnya
                //jika n akan akan eksekusi diluar perulangan karena tidak sesuai kondisi semuanya
            } while(ulangMenu != 'n' && ulangMenu == 'y');//mengulangi karena bernilai selain y && y (y mengulangin kalau n langsung ke bawah)
            float totalOrang = totalLaki + totalPerempuan + totalRemaja + totalAnak + totalBalita;
            float porsiPerempuan;
            if (totalOrang > 5.0F && totalOrang % 2.0F == 1.0F) {//totalOrang % 2.0F == 1.0F (bilangan yang dibagi 2 menghasilkan sisa yaitu 1)(bilangan ganjil < 5)
                porsiPerempuan = totalPerempuan * 2.0F;
            } else {
                porsiPerempuan = totalPerempuan * 1.0F;
            }//karena bagian perempuan perlu dieksekusi kusus perlu adanya percabangan
            float porsiLaki = totalLaki * 2.0F;
            float porsiAnak = totalAnak * 1.0F / 2.0F;
            float porsiBalita = totalBalita * 1.0F;
            float porsiRemaja = totalRemaja * 1.0F;
            float totalPorsi = porsiLaki + porsiPerempuan + porsiAnak + porsiBalita + porsiRemaja;
            System.out.println("\nJumlah laki-laki dewasa: " + (int)totalLaki);//int karena orang ngk mungkin ada koma nya
            System.out.println("Jumlah perempuan dewasa: " + (int)totalPerempuan);
            System.out.println("Jumlah remaja: " + (int)totalRemaja);
            System.out.println("Jumlah anak-anak: " + (int)totalAnak);
            System.out.println("Jumlah balita: " + (int)totalBalita);
            System.out.println("Total Orang: " + (int)totalOrang);
            System.out.println("Total Porsi: " + totalPorsi);
    }
}
