package Soalbaru;
import java.util.Scanner;
public class no8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Jumlah n = ");
        int batasan = input.nextInt();
        int fibonaci = 1;
        int bilAwal = 0;
        int bilAkhir = 1;//simpan nilai 1, hubungannya untuk mencari fibonanci
        int helper = 0;
        int[] array = new int[batasan];
        int[] arrayFibonaci = new int[batasan];
        int[] arrayPrima = new int[batasan];
        int[] arraySum = new int[batasan];
        int i;
        for(i = 0; i < array.length; ++i) {//masuk perulangan pada jumlah n yang ditampung pada array
            System.out.print(fibonaci + " ");//untuk menentukan fibonanci
            arrayFibonaci[i] = fibonaci;
            //rumus fibonanci
            fibonaci = bilAwal + bilAkhir;
            bilAwal = bilAkhir;
            bilAkhir = fibonaci;
        }
        System.out.println();
        i = 1;
        //Prima
        while(true) {//mendapatkan bilangan prima dan panjang array yang di jumlahin
            int number = 0;//helper untuk menyimpan angka bilangan prima
            for(int j = 1; j <= i; ++j) {
                if (i % j == 0) {//bilangan prima
                    ++number;
                    //System.out.println(number);
                }
            }
            if (number == 2) {//bilangan prima hanya dibagi dengan dirinya atau 1
                arrayPrima[helper] = i;
                ++helper;
                System.out.print(i + " ");
                if (helper == array.length) {
                    System.out.println();
                    System.out.println();
                    for(i = 0; i < array.length; ++i) {//penjumlahan 2 aray fibonanci dan prima
                        arraySum[i] = arrayFibonaci[i] + arrayPrima[i];
                        System.out.print(arraySum[i] + " ");
                    }
                    return;
                }
            }
            ++i;//menambahkan nilai i
        }
    }
}
