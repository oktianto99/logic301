package Soalbaru;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class no2 {
    public static void main(String[] args) {//membedakan jumlah vokal dan kongsonan(selain vokal)
        /*try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Masukkan text : ");
            String x = br.readLine();
            System.out.println();
            int v = 0, c = 0;
            for (int i=0; i<x.length(); i++) {
                if (checkVC(x.charAt(i))==true) {
                    if (getVC(x.charAt(i))==1)
                        v++;
                    else
                        c++;
                }
            }
            System.out.println("Jumlah huruf vokal : " + v);
            System.out.println("Jumlah huruf konsonan : " + c);
            System.out.println("Total huruf : " + (v+c));
        }
        catch (IOException ioe) {
            System.out.println("Error IOException : " + ioe.getMessage());
        }
    }
    private static boolean checkVC(char text) {

        if ((text>=65 && text<=90) || (text>=97 && text<=122))
            return true;
        else
            return false;
    }
    private static int getVC(char text) {
        int vc;
        if (text=='A' || text=='E' || text=='I' || text=='O' || text=='U'
                || text=='a' || text=='e' || text=='i' || text=='o' || text=='u')
            vc = 1;
        else
            vc = 0;
        return vc;*/
        char[] vokal = new char[]{'a', 'i', 'u', 'e', 'o'};
        char[] konsonan = new char[]{'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y', 'z'};
        //kalimat yang mengabaikan spasi karena huruf alpabed terbentuknya tidak ada spaci atau semacam lainnya (hanya menggunakan alpabet)
        Scanner input = new Scanner(System.in);
        System.out.println("Soal 02: Vokal dan Konsonan");
        System.out.print("Masukan kalimat : ");
        String stringInput = input.nextLine();
        String stringConvert = stringInput.toLowerCase();//toLowerCase = merubah huruf menjadi kecil
        char[] charArray = stringConvert.toCharArray();//toCharArray= merubah kalimat menjadi perkata
        Arrays.sort(charArray);//mengurutkan abjad yang acak menjadi rapi
        String vokalString = "";
        String konsonanString = "";
        for(int i = 0; i < charArray.length; ++i) {//lengh untuk panjang baris pada charArray
            //for awal mencari perkata yang sudah disusun rapi pada suatu kalimat (ceck perkata)
            int j;
            for(j = 0; j < vokal.length; ++j) {//masuk vokal
                if (vokal[j] == charArray[i]) {//ceck aray vokal dengan karakter pada suatu kalimat
                    vokalString = vokalString + charArray[i];//karakter disimpan pada vokal
                }
            }
            for(j = 0; j < konsonan.length; ++j) {
                if (konsonan[j] == charArray[i]) {
                    konsonanString = konsonanString + charArray[i];
                }
            }
        }
        System.out.print("Huruf Vokal: " + vokalString);
        System.out.println();
        System.out.print("Huruf Konsonan: " + konsonanString);
    }
}
