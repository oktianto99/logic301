package Logic01;
import java.util.Scanner;
public class Soal07 {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            // Nomor 07
            // Prompt input
            System.out.println("Please enter the value of n: ");
            int n = input.nextInt();

            // Prerequisite
            int[] arrayInteger = new int[n];
            int helper = 2;

            // Enter the value to array variable
            for (int i = 0; i < n; i++) {
                arrayInteger[i] = helper;
                helper *= 2;
                //arrayInteger1[2] = helper;

            }

            // Print array
            for (int i = 0; i < n; i++) {
                //System.out.print(arrayInteger[i] + " ");
                System.out.print(arrayInteger[i] + " ");
            }
        }
    }
}
