package Logic01;
import java.util.Scanner;
public class Soal11 {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            // Nomor 11
            // Prompt input
            System.out.println("Please enter the value of n: ");
            int n = input.nextInt();

            // Prerequisite
            int[] arrayInteger = new int[n];
            int helper = 0, helper1=1, helper2 = 1;

            // Enter the value to array variable
            for (int i = 0; i < n; i++) {
                arrayInteger[i] = helper2;
                helper2 = helper + helper1;
                helper=helper1;
                helper1=helper2;
            }

            // Print array
            for (int i = 0; i < n; i++) {
                System.out.print(arrayInteger[i] + " ");
            }
        }
    }
}
