package Logic01;
import java.util.Scanner;
public class Soal08 {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            // Nomor 08
            // Prompt input
            System.out.println("Please enter the value of n: ");
            int n = input.nextInt();

            // Prerequisite
            int[] arrayInteger = new int[n];
            int helper = 3;

            // Enter the value to array variable
            for (int i = 0; i < n; i++) {
                arrayInteger[i] = helper;
                helper = helper * 3;
                //arrayInteger1[2] = helper;

            }

            // Print array
            for (int i = 0; i < n; i++) {
                //System.out.print(arrayInteger[i] + " ");
                System.out.print(arrayInteger[i] + " ");
            }
        }
    }
}
