package Logic01;
import java.util.Scanner;
public class Soal05 {
    public static void main(String[] args) {
        try (Scanner input = new Scanner(System.in)) {
            // Nomor 05
            // Prompt input
            System.out.println("Please enter the value of n: ");
            int n = input.nextInt();

            // Prerequisite
            int[] arrayInteger = new int[n];
            int helper = 1;

            // Enter the value to array variable
            for (int i = 0; i < n; i++) {
                arrayInteger[i] = helper;
                helper += 4;
                //arrayInteger1[2] = helper;

            }

            // Print array
            for (int i = 0; i < n-2; i++) {
                //System.out.print(arrayInteger[i] + " ");
                if(i%2==0) {
                    System.out.print(arrayInteger[i] + " ");
                }
                else {
                    System.out.print(arrayInteger[i] + " * ");
                }
            }
        }
    }
}
